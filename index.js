#!/usr/bin/env node

"use strict";

let http = require('http');
//let qs = require('querystring');
// MongoDB connection
let MongoClient = require('mongodb').MongoClient
let assert = require('assert');

// Connection URL
let mongourl = 'mongodb://clacuser:clacclac@ds127854.mlab.com:27854/clacdoigtsdb';

// DB Instance
let targetdb;

let port = process.env.PORT || 3000;

// Request-Promise for server data requests
let rp = require('request-promise');

const API_ADDRESS = 'http://163.172.145.75';
const MY_SERVER_TOKEN = 'k-0dkjklasdf99ikjasdf';

// MongoDB connect method
MongoClient.connect(mongourl, (err, db) => {
  assert.equal(null, err);
  console.log("Connected to MLab MongoDB server with success!");
  targetdb = db;
});

// Get my credentials (token) for this session using promises
// And setup Webhook to get intent from NODE-02 server

rp({
    method: 'POST',
    uri: API_ADDRESS + '/Candidats/login',
    json: true,
    body: {
      username: "vsiguero",
      password: "clacdoigts"
    }
  })
  .then((access_token) => {
    //console.log(access_token);
    let webhook = {
      uri: API_ADDRESS + '/Candidats/' + access_token.userId + '/webhooks?access_token=' + access_token.id,
      data: {
        "token": MY_SERVER_TOKEN,
        // Building in heroku
        "url": "https://ccdtest.herokuapp.com/nlpid"
      }
    }
    //console.log(webhookuri);
    // I send a POST with my credentials in order to setup a Webhook
    // into NLP-NODE02 server
    return rp({
      method: 'POST',
      uri: webhook.uri,
      body: webhook.data,
      json: true,
      headers: {
        'content-type': 'application/json'
      }
    });
  })
  .then((message) => {
    // Do more things to setup Webhook?
    console.log('NLP-NODE02 response: ');
    console.dir(message);
  })
  .catch((err) => {
    // Request failed...
    console.log(err.message);
  });

// Set-up NPL-NODE02 Webhook callback url @ http://localhost:3000/nlpid
let server = http.createServer((req, res) => {
  if (req.url != '/nlpid') return res.end();
  // Protect the Webhook post with TOKEN from header
  if (req.method == 'POST' && req.headers.token == MY_SERVER_TOKEN) {
    console.log("Headers: " + req.headers.token);
    req.on('data', (data) => {
      console.log('data:' + data);
      updateData(targetdb, JSON.parse(data), (result) => {
        //console.log(result);
      });
      res.write('200 Webhook received from nlp_id: ' + data);
      res.end();
    });
  } else {
    // Send 4XX or 5XX Headers with response...
    res.write('Token error!!');
    res.end();
  }
});

server.listen(port);

// EventSource for processing the SSE in NODE01 server
let EventSource = require('eventsource');
let es = new EventSource(API_ADDRESS + '/Messages/change-stream');

es.onopen = function (evt) {
  console.log(evt);
};

es.addEventListener('data', (evt) => {
  //console.log(evt.data);
  // TODO: We wait for the credentials & webkhook setup to be finished
  // then we start to process events...
  let jsondata = JSON.parse(evt.data);
  console.log('data: ' + jsondata.data.id);
  // If we have an event, resend to the server to get the full message
  rp({
      uri: API_ADDRESS + '/Messages/' + jsondata.data.id,
      json: true
    })
    .then((message) => {
      // Insert data into MongoDB...
      //console.log(message);
      // We save the message into mongodb's collection as soon as we have it
      insertData(targetdb, message, (result) => {
        console.log('insertedId: ' + result.insertedId);
      });
    })
    .catch((err) => {
      // Request failed...
    });

});

es.onerror = function (err) {
  console.log(err);
};


// InsertData function for MongoDB

let insertData = (db, data, callback) => {
  // Get the documents collection
  let collection = db.collection('mergedresults');
  // Insert some documents
  collection.insertOne(data, (err, result) => {
    //assert.equal(err, null);
    //assert.equal(1, result.insertedCount);
    console.log("Inserted flux into the mergedresults document collection");
    // Dont forget to close db...
    //db.close();
    callback(result);
  });
}

// UpdateData function for MongoDB

let updateData = function (db, data, callback) {
  // Get the documents collection
  let collection = db.collection('mergedresults');
  // Update document
  let query = {
    nlp_id: data.nlp_id
  };
  let change = {
    $set: {
      intent: data.intent
    },
    $unset: {
      nlp_id: 1
    }
  };
  collection.updateOne(query, change, function (err, result) {
    if (err) throw err;
    // TODO: Check for errors on updating collection...
    //assert.equal(err, null);
    //assert.equal(1, result.modifiedCount);
    //console.dir(result);
    console.log("Merged flux '" + data.nlp_id + "' with result '" + result.modifiedCount + "' into mergedresults document collection");
    // Dont forget to close db...
    //db.close();
    callback(result);
  });
}